var x = 150;
var y = 150;
var paddlex;
var paddleh;
var paddlew;
var dx = 2;
var dy = 4;
var WIDTH;
var HEIGHT;
var canvasMinX = 0;
var canvasMaxX = 0;
var intervalid = 0;
var ctx;
var bricks;
var NROWS;
var NCOLS;
var BRICKWIDTH;
var BRICKHEIGHT;
var PADDING;
var ballr = 10;
var rowcolors = ["red", "orange", "yellow", "blue", "green", "indigo", "purple"];
var paddlecolor = "#FFFFFF";
var ballcolor = "#FFFFFF";
var backcolor = "#000000";
var cleared = 0;

rightdown = false;
leftdown = false;

function onKeyDown (evt) {
	if (evt.keyCode == 39) rightdown = true;
	if (evt.keyCode == 37) leftdown = true;
}

function onKeyUp (evt) {
	if (evt.keyCode == 39) rightdown = false;
	if (evt.keyCode == 37) leftdown = false;
}

$(document).keydown(onKeyDown);
$(document).keyup(onKeyUp);

function init() {
	ctx = $("#canvas")[0].getContext("2d");
	WIDTH = $("#canvas").width();
	HEIGHT = $("canvas").height();
	intervalId = setInterval(draw, 10);
}

function init_paddle() {
	paddlex = WIDTH / 2;
	paddleh = 15;
	paddlew = 100;
}

function initbricks() {
	NROWS = 7;
	NCOLS = 10;
	BRICKWIDTH = (WIDTH / NCOLS) - 1;
	BRICKHEIGHT = 20;
	PADDING = 1;
	
	bricks = new Array(NROWS);
	for(i = 0; i < NROWS; i++) {
		bricks[i] = new Array(NCOLS);
		for (j = 0; j < NCOLS; j++) {
			bricks[i][j] = 1;
		}
	}
}

function drawbricks() {
	for(i = 0; i < NROWS; i++) {
		for (j = 0; j < NCOLS; j++) {
			if (bricks[i][j] == 1){
				ctx.fillStyle = rowcolors[i];
				rect((j * (BRICKWIDTH + PADDING)) + PADDING,
				(i * (BRICKHEIGHT + PADDING)) * PADDING,
				BRICKWIDTH, BRICKHEIGHT)
			}
		}
	}
}

function circle(x, y, r) {
	ctx.beginPath();
	ctx.arc(x, y, r, 0, Math.PI * 2, true);
	ctx.closePath();
	ctx.fill();
}

function rect(x, y, w, h) {
	ctx.beginPath();
	ctx.rect(x, y, w, h);
	ctx.closePath();
	ctx.fill();
}

function clear() {
	ctx.clearRect(0, 0, WIDTH, HEIGHT);
	rect(0, 0, WIDTH, HEIGHT);
}