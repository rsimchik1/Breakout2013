var draw = function() {
	ctx.fillStyle = backcolor;
	clear();
	ctx.fillStyle = ballcolor;
	circle(x, y, ballr);
	
	if (rightdown && paddlex !== WIDTH - paddlew) paddlex += 5;
	else if (leftdown && paddlex !== 0) paddlex -= 5;
	ctx.fillStyle = paddlecolor;
	rect(paddlex, HEIGHT - paddleh, paddlew, paddleh);
	
	drawbricks();
	
	rowheight = BRICKHEIGHT + PADDING;
	colwidth = BRICKWIDTH + PADDING;
	row = Math.floor(y/rowheight);
	col = Math.floor(x/colwidth);
	
	if (y < NROWS * rowheight && row >= 0 && col >= 0 && bricks[row][col] == 1) {
		dy = -dy;
		bricks[row][col] = 0;
		cleared++;
	}
	
	if (x + dx + ballr > WIDTH || x + dx - ballr < 0)
	dx = -dx;
	
	if (y + dy - ballr < 0)
		dy = -dy;
	else if (y + dy + ballr > HEIGHT - paddleh){
		if (x > paddlex && x < paddlex + paddlew) {
			dx = 8 * ((x - (paddlex + paddlew/2)) / paddlew);
			dy = -dy;
			}
		else if (y + dy + ballr > HEIGHT) {
			clearInterval(intervalId);
			ctx.fillStyle = "black";
			clear();
			ctx.fillStyle = "red";
			ctx.font = "100px Verdana";
			ctx.fillText("You Lose!",WIDTH / 2 - 240,HEIGHT / 2 + 25);
			ctx.lineWidth = 10;
			ctx.strokeStyle = "red";
			ctx.strokeRect(30, 180, 540, 220);
		}
	}
	
	if (cleared == NROWS * NCOLS){
		ctx.font="50px Verdana";
		clearInterval(intervalId);
		ctx.fillStyle = "black";
		clear();
		ctx.fillStyle = "blue";
		ctx.font = "100px Verdana";
		ctx.fillText("You Win!",WIDTH / 2 - 220,HEIGHT / 2 + 25);
		ctx.lineWidth = 10;
		ctx.strokeStyle = "blue";
		ctx.strokeRect(50, 190, 500, 200);
	};
	
	x += dx;
	y += dy;
}

var initgame = function(){init();
init_paddle();
initbricks();
}